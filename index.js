const getUsers = fetch('https://ajax.test-danit.com/api/json/users');
const getPosts = fetch('https://ajax.test-danit.com/api/json/posts');
const token = 'e04ca2d9-271c-47a1-af9f-15a81723cb57';

class Card{
    constructor(fullName, email, title, bodyPost){
        this.fullName = fullName;
        this.email = email;
        this.title = title;
        this.bodyPost = bodyPost;
    }

}

document.querySelector(".loader").style.display = "block";
document.querySelector(".cards").style.display = "none";
function post(users, posts){
    posts.forEach(post => {
        let _name = users.find(obj => obj.id === post.userId).name;
        let _email = users.find(obj => obj.id === post.userId).email;
        let card = new Card(_name, _email, post.title, post.body);
        let div_post = document.createElement('div');
        div_post.classList.add('card_post');

        let div_user = document.createElement('div');
        div_user.classList.add('user');

        let div_userName = document.createElement('div');
        div_userName.classList.add('userName');
        div_userName.textContent = card.fullName;
        div_user.appendChild(div_userName);

        let div_userEmail = document.createElement('div');
        div_userEmail.classList.add('email');
        div_userEmail.textContent = card.email;
        div_user.appendChild(div_userEmail);

        div_post.appendChild(div_user);

        let div_titlePost = document.createElement('div');
        div_titlePost.classList.add('title');
        div_titlePost.textContent = card.title;
        div_post.appendChild(div_titlePost);

        let div_bodyPost = document.createElement('div');
        div_bodyPost.classList.add('body_post');
        div_bodyPost.textContent = card.body;
        div_post.appendChild(div_bodyPost);

        let btn_delete = document.createElement('button');
        btn_delete.classList.add('btn');
        btn_delete.classList.add('btn-danger');
        btn_delete.textContent = 'DELETE';
        div_post.appendChild(btn_delete);

        btn_delete.addEventListener('click', () => {
            console.log('Натиснута кнопка DELETE, id = ', post.id, `https://ajax.test-danit.com/api/json/posts/${post.id}`);
            fetch(`https://ajax.test-danit.com/api/json/posts/${post.id}`, {
                method: 'DELETE'
            })
            .then(response => {
                if(response.status == 200)
                    console.log('ok');
                    div_post.remove();
            });
          });

        document.querySelector('.cards').appendChild(div_post);
    });
}
Promise.all([getUsers, getPosts])
  .then(responses => {
    const [usersResponse, postsResponse] = responses;

    if (!usersResponse.ok) {
      throw new Error('Помилка при отриманні користувачів');
    }
    if (!postsResponse.ok) {
      throw new Error('Помилка при отриманні постів');
    }

    return Promise.all([usersResponse.json(), postsResponse.json()]);
  })
  .then(data => {
    document.querySelector(".loader").style.display = "none";
    document.querySelector(".cards").style.display = "block";
    const [users, posts] = data;
    post(users,posts);
    console.log('Користувачі:', users);
    console.log('Пости:', posts);
  })
  .catch(error => {
    console.error('Сталася помилка:', error);
  });